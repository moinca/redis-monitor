package com.opensource.nredis.proxy.monitor.dao;

import com.opensource.nredis.proxy.monitor.model.AutoPageSystemQrtzTriggerInfo;


/**
* dao
*
* @author liubing
* @date 2016/12/30 08:41
* @version v1.0.0
*/
public interface IAutoPageSystemQrtzTriggerInfoDao extends IMyBatisRepository<AutoPageSystemQrtzTriggerInfo>,IPaginationDao<AutoPageSystemQrtzTriggerInfo> {
}
