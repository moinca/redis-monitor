package com.opensource.nredis.proxy.monitor.dao;

import com.opensource.nredis.proxy.monitor.model.PsUser;


/**
* dao
*
* @author liubing
* @date 2016/12/12 12:20
* @version v1.0.0
*/
public interface IPsUserDao extends IMyBatisRepository<PsUser>,IPaginationDao<PsUser> {
}
