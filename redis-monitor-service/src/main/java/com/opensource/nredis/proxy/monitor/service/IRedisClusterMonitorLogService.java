package com.opensource.nredis.proxy.monitor.service;

import com.opensource.nredis.proxy.monitor.model.RedisClusterMonitorLog;

/**
* service interface
*
* @author liubing
* @date 2017/01/11 12:18
* @version v1.0.0
*/
public interface IRedisClusterMonitorLogService extends IBaseService<RedisClusterMonitorLog>,IPaginationService<RedisClusterMonitorLog>  {
}

